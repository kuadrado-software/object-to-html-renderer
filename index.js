"use strict";

function autoid() {
    window.obj2html_auto_id = (window.obj2html_auto_id ?? 0) + 1;
    return "obj2html-autoid-" + String(window.obj2html_auto_id);
}

module.exports = {
    register_key: "objectToHtmlRender",

    /**
     * Register "this" as a window scope accessible variable named by the given key, or default.
     * @param {String} key 
     */
    register(key) {
        const register_key = key || this.register_key;
        window[register_key] = this;
    },

    /**
     * This must be called before any other method in order to initialize the lib.
     * It provides the root of the rendering cycle as a Javascript object.
     * @param {Object} renderCycleRoot A JS component with a render method.
     */
    setRenderCycleRoot(renderCycleRoot) {
        this.renderCycleRoot = renderCycleRoot;
    },

    event_name: "objtohtml-render-cycle",

    /**
     * Set a custom event name for the event that is trigger on render cycle.
     * Default is "objtohtml-render-cycle".
     * @param {String} evt_name 
     */
    setEventName(evt_name) {
        this.event_name = evt_name;
    },

    /**
     * This is the core agorithm that read an javascript Object and convert it into an HTML element.
     * @param {Object} obj The object representing the html element must be formatted like:
     * {
     *      tag: String // The name of the html tag, Any valid html tag should work. div, section, br, ul, li...
     *      xmlns: String // This can replace the tag key if the element is an element with a namespace URI, for example an <svg> tag.
     *                      See https://developer.mozilla.org/en-US/docs/Web/API/Document/createElementNS for more information
     *      style_rules: Object // a object providing css attributes. The attributes names must be in JS syntax,
     *                              like maxHeight: "500px", backgrouncColor: "#ff2d56",  margin: 0,  etc.
     *      contents: Array or String // This reprensents the contents that will be nested in the created html element.
     *                                   <div>{contents}</div>
     *                                   The contents can be an array of other objects reprenting elements (with tag, contents, etc)
     *                                   or it can be a simple string.
     *      // All other attributes will be parsed as html attributes. They can be anything like onclick, href, onchange, title...
     *      // or they can also define custom html5 attributes, like data, my_custom_attr or anything.
     * }
     * @returns {HTMLElement} The output html node.
     */
    objectToHtml(obj) {
        if (!obj) return document.createElement("span"); // in case of invalid input, don't block the whole process.
        const objectToHtml = this.objectToHtml.bind(this);
        const { tag, xmlns } = obj;
        const node = xmlns !== undefined ? document.createElementNS(xmlns, tag) : document.createElement(tag);
        const excludeKeys = ["tag", "contents", "style_rules", "state", "xmlns"];

        Object.keys(obj)
            .filter(attr => !excludeKeys.includes(attr))
            .forEach(attr => {
                switch (attr) {
                    case "class":
                        node.classList.add(...obj[attr].split(" ").filter(s => s !== ""));
                        break;
                    case "on_render":
                        if (!obj.id) {
                            node.id = autoid();
                        }
                        if (typeof obj.on_render !== "function") {
                            console.error("The on_render attribute must be a function")
                        } else {
                            this.attach_on_render_callback(node, obj.on_render);
                        }
                        break;
                    default:
                        if (xmlns !== undefined) {
                            node.setAttributeNS(null, attr, obj[attr])
                        } else {
                            if (typeof obj[attr] === "function") {
                                node[attr] = obj[attr];
                            } else {
                                // This doesn't work for function type attributes
                                node.setAttribute(attr, obj[attr]);
                            }
                        }
                }
            });
        if (obj.contents && typeof obj.contents === "string") {
            node.innerHTML = obj.contents;
        } else {
            obj.contents &&
                obj.contents.length > 0 &&
                obj.contents.forEach(el => {
                    switch (typeof el) {
                        case "string":
                            node.innerHTML = el;
                            break;
                        case "object":
                            if (xmlns !== undefined) {
                                el = Object.assign(el, { xmlns })
                            }
                            node.appendChild(objectToHtml(el));
                            break;
                    }
                });
        }

        if (obj.style_rules) {
            Object.keys(obj.style_rules).forEach(rule => {
                node.style[rule] = obj.style_rules[rule];
            });
        }

        return node;
    },

    on_render_callbacks: [],

    /**
     * This is called if the on_render attribute of a component is set.
     * @param {HTMLElement} node The created html element
     * @param {Function} callback The callback defined in the js component to render
     */
    attach_on_render_callback(node, callback) {
        const callback_handler = {
            callback: e => {
                if (e.detail.outputNode === node || e.detail.outputNode.querySelector(`#${node.id}`)) {
                    callback(node);
                    const handler_index = this.on_render_callbacks.indexOf((this.on_render_callbacks.find(cb => cb.node === node)));
                    if (handler_index === -1) {
                        console.warn("A callback was registered for node with id " + node.id + " but callbacck handler is undefined.")
                    } else {
                        window.removeEventListener(this.event_name, this.on_render_callbacks[handler_index].callback)
                        this.on_render_callbacks.splice(handler_index, 1);
                    }
                }
            },
            node,
        };

        const len = this.on_render_callbacks.push(callback_handler);
        window.addEventListener(this.event_name, this.on_render_callbacks[len - 1].callback);
    },

    /**
     * If a main element exists in the html document, it will be used as rendering root.
     * If not, it will be created and inserted.
     */
    renderCycle: function () {
        const main_elmt = document.getElementsByTagName("main")[0] || (function () {
            const created_main = document.createElement("main");
            document.body.appendChild(created_main);
            return created_main;
        })();

        this.subRender(this.renderCycleRoot.render(), main_elmt, { mode: "replace" });
    },

    /**
     * This method behaves like the renderCycle() method, but rather that starting the rendering cycle from the root component,
    * it can start from any component of the tree. The root component must be given as the first argument, the second argument be
    * be a valid html element in the dom and will be used as the insertion target.
     * @param {Object} object An object providing a render method returning an object representation of the html to insert
     * @param {HTMLElement} htmlNode The htlm element to update
     * @param {Object} options can be used the define the insertion mode, default is set to "append" and can be set to "override",
         * "insert-before" (must be defined along with an insertIndex key (integer)),
         * "adjacent" (must be defined along with an insertLocation key (String)), "replace" or "remove".
         * In case of "remove", the first argument "object" is not used and can be set to null, undefined or {}...
     */
    subRender(object, htmlNode, options = { mode: "append" }) {
        let outputNode = null;

        const get_insert = () => {
            outputNode = this.objectToHtml(object);
            return outputNode;
        };

        switch (options.mode) {
            case "append":
                htmlNode.appendChild(get_insert());
                break;
            case "override":
                htmlNode.innerHTML = "";
                htmlNode.appendChild(get_insert());
                break;
            case "insert-before":
                htmlNode.insertBefore(get_insert(), htmlNode.childNodes[options.insertIndex]);
                break;
            case "adjacent":
                /**
                 * options.insertLocation must be one of:
                 *
                 * afterbegin
                 * afterend
                 * beforebegin
                 * beforeend
                 */
                htmlNode.insertAdjacentHTML(options.insertLocation, get_insert().outerHTML);
                break;
            case "replace":
                htmlNode.parentNode.replaceChild(get_insert(), htmlNode);
                break;
            case "remove":
                htmlNode.remove();
                break;
        }
        const evt_name = this.event_name;
        const event = new CustomEvent(evt_name, {
            detail: {
                inputObject: object,
                outputNode,
                insertOptions: options,
                targetNode: htmlNode,
            }
        });

        this.asyncSubscriptions.length > 0 && this.clearAsyncSubscriptions(htmlNode);

        window.dispatchEvent(event);
    },

    /**
     * If a reference to an object representing a rendered component is passed to an asynchronous closure, 
     * the object may be referenced here with its id and a cleanup callback.
     * When rendering is called, if the object id is found in the rendering tree, the cleanup callback is called and the reference is removed.
     */
    asyncSubscriptions: [],
    /**
     * Should be called when an renderable object is referenced in an asynchronous closure.
     * @param {String} htmlNodeId The id of the htmlNode being represented by the object
     * @param {Function} cleanupCallback The callback that should handle the cleanup of the subscription
     */
    registerAsyncSubscription(htmlNodeId, cleanupCallback) {
        this.asyncSubscriptions.push({
            htmlNodeId,
            cleanupCallback,
        });
    },
    /**
     * Will call cleanup callback of previously referenced asynchronous subscriptions.
     * @param {HTMLElement} fromNode The node from which are rendered the elements
     */
    clearAsyncSubscriptions(fromNode) {
        const cleared = [];

        this.asyncSubscriptions.forEach(s => {
            if (fromNode.id === s.htmlNodeId || fromNode.querySelector(`#${s.htmlNodeId}`)) {
                s.cleanupCallback();
                cleared.push(s);
            }
        });

        this.asyncSubscriptions = this.asyncSubscriptions.filter(s => !cleared.includes(s));
    },
};
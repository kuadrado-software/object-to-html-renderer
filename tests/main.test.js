const renderer = require("..");
const createTestingContext = require("./create-testing-context");
const SimpleTestingMain = require("./components/simple-testing-main");
const TestingApp = require("./components/testing-app");


describe("Main tests", () => {
    createTestingContext();

    test("window registration", () => {
        renderer.register("obj2htm");
        expect(window.obj2htm).toBeDefined();
    });

    test("root component registration", () => {
        const root_cpt = new SimpleTestingMain();
        renderer.setRenderCycleRoot(root_cpt)
        expect(renderer.renderCycleRoot).toEqual(root_cpt);
    });

    test("Render cycle", () => {
        const root_cpt = new SimpleTestingMain("Hello testing");
        renderer.setRenderCycleRoot(root_cpt);
        renderer.renderCycle();
        const main_el = document.querySelector("main");
        expect(main_el).toBeDefined();
        expect(main_el.innerHTML).toBe("Hello testing");
    });

    test("Element onclick", () => {
        const app = new TestingApp({
            button_clicked: false,
            button_text: "test button"
        });
        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const app_el = document.querySelector("#testing-app");
        expect(app_el).toBeDefined();

        const button = app_el.querySelector("#test-button");
        expect(button).toBeDefined();
        expect(button.innerHTML).toBe("test button");

        button.click();
        expect(app.testing_data.button_clicked).toBe(true);
    });
})


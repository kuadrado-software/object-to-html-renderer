const SimpleTestingMain = require("./components/simple-testing-main");
const renderer = require("..");
const createTestingContext = require("./create-testing-context");

createTestingContext();

test("render event", done => {
    const root_cpt = new SimpleTestingMain("Hello testing");
    renderer.setRenderCycleRoot(root_cpt);

    window.addEventListener(renderer.event_name, e => {
        try {
            const output = e.detail.outputNode;
            expect(output).toBeDefined();
            expect(output.innerHTML).toBe("Hello testing");
            done();
        } catch (error) {
            done(error)
        }
    });

    renderer.renderCycle();
});
"use strict";

class SimpleTestingMain {
    constructor(text) { this.text = text }
    render() {
        return {
            tag: "main",
            contents: this.text
        }
    }
}

module.exports = SimpleTestingMain;
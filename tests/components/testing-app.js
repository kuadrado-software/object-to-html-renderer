"use strict";

class TestingApp {
    constructor(testing_data) {
        this.testing_data = testing_data;
    }

    render() {
        return {
            tag: "main",
            id: "testing-app",
            style_rules: this.testing_data.style,
            contents: [
                {
                    tag: "button",
                    id: "test-button",
                    contents: this.testing_data.button_text,
                    onclick: () => this.testing_data.button_clicked = true,
                },
                {
                    tag: "div",
                    id: "test-div",
                    contents: [
                        {
                            tag: "p",
                            id: "test-paragraph",
                            contents: this.testing_data.paragraph_text,
                            on_render: () => this.testing_data.paragraph_rendered = true,
                        }
                    ]
                },
                this.testing_data.custom_contents && {
                    tag: "div",
                    id: "custom-content-container",
                    contents: this.testing_data.custom_contents,
                    on_render: () => this.testing_data.custom_contents_renderer = true,
                }
            ]
        }
    }
}

module.exports = TestingApp;
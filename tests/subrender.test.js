const createTestingContext = require("./create-testing-context");
const TestingApp = require("./components/testing-app");
const renderer = require("..");

describe("Subrender tests", () => {
    createTestingContext();

    test("Subrender append", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });
        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const p = document.querySelector("#test-paragraph");
        expect(p).not.toBeNull();
        expect(p.innerHTML).toBe("test paragraph");

        renderer.subRender(
            { tag: "span", id: "new-span", contents: "new" }, p
        );
        const new_span = p.querySelector("#new-span");
        expect(new_span).not.toBeNull();
        expect(new_span.innerHTML).toBe("new");
    });

    test("Subrender override", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });
        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const div = document.querySelector("#test-div");
        const p = div.querySelector("p");
        expect(p).not.toBeNull();

        renderer.subRender(
            { tag: "em", id: "test-override", contents: "overriden" },
            div,
            { mode: "override" }
        );

        const new_content = div.querySelector("em");
        expect(new_content).not.toBeNull();
        expect(new_content.innerHTML).toBe("overriden");
        expect(div.innerHTML).toBe(new_content.outerHTML);
    });

    test("Subrender insert before", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });
        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const div = document.querySelector("#test-div");
        expect(div.childNodes.length).toBe(1);

        const insert_span_obj_1 = { tag: "span", id: "insert-span-1", contents: "inserted first" };
        const insert_span_obj_2 = { tag: "span", id: "insert-span-2", contents: "inserted last" };

        renderer.subRender(
            insert_span_obj_1,
            div,
            { mode: "insert-before", insertIndex: 0 }
        );

        expect(div.childNodes.length).toBe(2);

        let inserted = document.querySelector("#insert-span-1");
        expect(inserted).not.toBeNull();
        expect(inserted.innerHTML).toBe("inserted first");
        expect(div.childNodes[0]).toEqual(inserted);

        renderer.subRender(
            insert_span_obj_2,
            div,
            { mode: "insert-before", insertIndex: 1 }
        );

        expect(div.childNodes.length).toBe(3);

        inserted = document.querySelector("#insert-span-2");
        expect(inserted).not.toBeNull();
        expect(inserted.innerHTML).toBe("inserted last");
        expect(div.childNodes[1]).toEqual(inserted);
    });

    test("Subrender adjacent", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });

        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const div = document.querySelector("#test-div");

        const insert_before_begin = { tag: "span", id: "insert-before-begin", contents: "before begin" };
        const insert_after_begin = { tag: "span", id: "insert-after-begin", contents: "after begin" };
        const insert_before_end = { tag: "span", id: "insert-before-end", contents: "brefore end" };
        const insert_after_end = { tag: "span", id: "insert-after-end", contents: "after end" };

        // before begin
        renderer.subRender(
            insert_before_begin,
            div,
            { mode: "adjacent", insertLocation: "beforebegin" }
        );

        let inserted = document.querySelector("#insert-before-begin");
        expect(inserted).not.toBeNull();
        expect(div.previousElementSibling).toEqual(inserted);

        // after begin
        renderer.subRender(
            insert_after_begin,
            div,
            { mode: "adjacent", insertLocation: "afterbegin" }
        );

        inserted = document.querySelector("#insert-after-begin");
        expect(inserted).not.toBeNull();
        expect(div.childNodes[0]).toEqual(inserted);

        // before end
        renderer.subRender(
            insert_before_end,
            div,
            { mode: "adjacent", insertLocation: "beforeend" }
        );

        inserted = document.querySelector("#insert-before-end");
        expect(inserted).not.toBeNull();
        expect(div.childNodes[div.childNodes.length - 1]).toEqual(inserted);

        // after end
        renderer.subRender(
            insert_after_end,
            div,
            { mode: "adjacent", insertLocation: "afterend" }
        );

        inserted = document.querySelector("#insert-after-end");
        expect(inserted).not.toBeNull();
        expect(div.nextElementSibling).toEqual(inserted);
    });

    test("Subrender replace", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });
        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const p = document.querySelector("#test-paragraph");

        renderer.subRender(
            { tag: "p", id: "test-paragraph", contents: "changed" },
            p,
            { mode: "replace" }
        );

        expect(document.querySelector("#test-paragraph").innerHTML).toBe("changed");
    });

    test("Subrender remove", () => {
        const app = new TestingApp({
            paragraph_text: "test paragraph"
        });

        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const p = document.querySelector("#test-paragraph");

        renderer.subRender(
            { tag: "span" },
            p,
            { mode: "remove" }
        );

        expect(document.querySelector("#test-paragraph")).toBeNull();
    });
});

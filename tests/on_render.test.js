const renderer = require("..");
const TestingApp = require("./components/testing-app");
const createTestingContext = require("./create-testing-context");

createTestingContext();

test("on_render callback", () => {
    const app = new TestingApp({ paragraph_rendered: false });

    renderer.setRenderCycleRoot(app);
    renderer.renderCycle();

    expect(app.testing_data.paragraph_rendered).toBe(true);
});
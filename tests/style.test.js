
const renderer = require("..");
const createTestingContext = require("./create-testing-context");
const TestingApp = require("./components/testing-app");

createTestingContext();

test("component styling", () => {
    const app = new TestingApp({
        style: {
            backgroundColor: "blue"
        }
    });

    renderer.setRenderCycleRoot(app);
    renderer.renderCycle();
    const el = document.querySelector("main");
    expect(el.style.backgroundColor).toBe("blue");
});
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = function () {
    const base_html = `<!DOCTYPE html><html><body></body></html>`;
    const DOM = new JSDOM(base_html);
    const { window } = DOM;
    const { document } = window;
    global.window = window;
    global.document = document;
    global.CustomEvent = window.CustomEvent;
};
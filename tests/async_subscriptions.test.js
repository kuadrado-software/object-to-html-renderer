const TestingApp = require("./components/testing-app");
const createTestingContext = require("./create-testing-context");
const renderer = require("..");

class ComponentSubscribesSetInterval {
    constructor(interval_state) {
        this.interval_state = interval_state;
        this.count = 0;
        this.id = "setintervalcpt";
        this.run_interval();
    }

    run_interval() {
        this.interval_state.id = setInterval(() => this.count++, 1000);

        renderer.registerAsyncSubscription(this.id, this.clear_interval.bind(this));
    }

    clear_interval() {
        clearInterval(this.interval_state.id);
        delete this.interval_state.id;
    }

    render() {
        return {
            tag: "div",
            id: this.id,
        }
    }
}

class ComponentSubscribesTimeout {
    constructor(timeout_state) {
        this.timeout_state = timeout_state;
        this.count = 0;
        this.id = "timeoutcpt";
        this.run_timeout();
    }

    run_timeout() {
        this.timeout_state.id = setTimeout(() => this.count++, 5000);

        renderer.registerAsyncSubscription(this.id, this.clear_timeout.bind(this));
    }

    clear_timeout() {
        clearTimeout(this.timeout_state.id);
        delete this.timeout_state.id;
    }

    render() {
        return {
            tag: "div",
            id: this.id,
        }
    }
}


describe("Async subscriptions tests", () => {
    createTestingContext();

    it("should register and clean setinterval subscription", () => {
        let render_setint_cpt = true;
        const interval_state = {};

        const get_custom_contents = () => {
            return {
                tag: "div",
                id: "custom-contents",
                contents: render_setint_cpt
                    ? [new ComponentSubscribesSetInterval(interval_state).render()]
                    : [{ tag: "span" }]
            }
        };

        const app = new TestingApp({
            custom_contents: [get_custom_contents()],
        });

        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const setint_cpt = document.querySelector("#setintervalcpt");
        expect(setint_cpt).not.toBeNull();
        expect(renderer.asyncSubscriptions.find(s => s.htmlNodeId === "setintervalcpt")).toBeDefined();
        expect(interval_state.id).toBeDefined();


        // Recreate the custom contents, will destroy the previous one. Async subscription should be cleaned up.
        render_setint_cpt = false;
        renderer.subRender(get_custom_contents(), document.querySelector("#custom-contents"), { mode: "replace" });
        expect(interval_state.id).not.toBeDefined();
    });


    it("should register and clear timeout subscription", () => {
        let render_timeout_cpt = true;
        const timeout_state = {};

        const get_custom_contents = () => {
            return {
                tag: "div",
                id: "custom-contents",
                contents: render_timeout_cpt
                    ? [new ComponentSubscribesTimeout(timeout_state).render()]
                    : [{ tag: "span" }]
            }
        };

        const app = new TestingApp({
            custom_contents: [get_custom_contents()],
        });

        renderer.setRenderCycleRoot(app);
        renderer.renderCycle();

        const to_cpt = document.querySelector("#timeoutcpt");
        expect(to_cpt).not.toBeNull();
        expect(renderer.asyncSubscriptions.find(s => s.htmlNodeId === "timeoutcpt")).toBeDefined();
        expect(timeout_state.id).toBeDefined();


        // Recreate the custom contents, will destroy the previous one. Async subscription should be cleaned up.
        render_timeout_cpt = false;
        renderer.subRender(get_custom_contents(), document.querySelector("#custom-contents"), { mode: "replace" });
        expect(timeout_state.id).not.toBeDefined();
    });



})
